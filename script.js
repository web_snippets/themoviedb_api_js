//API for https://developers.themoviedb.org/3/getting-started/introduction
//api_key:ed6ec98d731aed8fb1043824d582950e


const searchForm = document.querySelector('#search-Form');
const movie = document.querySelector('#movies');


//Берем запрос с формы и передаем 
function apiSearch()
{
    const searchText = document.querySelector('.form-control').value;
    const server = 'https://api.themoviedb.org/3/search/multi?api_key=API_KEY&language=ru&query='+searchText;
    /*
    Метод fetch – это XMLHttpRequest нового поколения. Он предоставляет улучшенный интерфейс для осуществления запросов к серверу: как по части возможностей и контроля над происходящим, так и по синтаксису, так как построен на промисах.
    */
    fetch(server)
    .then(
        function(value)
        {
            if (value.status !== 200)
            {
                return Promise.reject(new Error('Ошибка'))
            }
            return value.json();
        })
    .then(function(output)
        {
            outViewPage(output)
        })
    .catch(function(reason)
    {
        errorViewPage(reason)
    })
}
searchForm.addEventListener('submit',event=>{event.preventDefault(),apiSearch()});


///Добавил функцию вывода на экран в красивой форме
function outViewPage(output)
{
    let inner = '';
    output.results.forEach(function(item)
        {
            let nameItem;
            let descr;
            let poster;
            let poster_src;
            let date_start;
            nameItem = item.name || item.title;
                    
                    descr = item.overview;
                    poster = item.poster_path;
                    if (poster == null)
                        poster_src = '/not_found.png'
                    else
                        poster_src = `https://image.tmdb.org/t/p/w300${poster}`
            switch(item.media_type)
            {
                case "movie":
                    date_start = item.release_date;
                    break;
                case "tv":
                    date_start = item.first_air_date;
                    break;
                    default:
                        date_start = 'Неизвестно'
            }

            inner +=  `
            <div class="col-12 col-md-4 col-xs-3"> 
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="${poster_src}" alt="">
                    <div class="card-body">
                        <h3 class="card-title">${nameItem}</h3>
                        <p class="card-text">${descr}</p>
                        <h5> Статистика</h5>
                        <p>Дата релиза:${date_start}</p>
                        <p>Рейтинг:${item.vote_average}</p>
                        <p>Количество голосов: ${item.vote_count}</p>
                        <p>Страна: ${item.origin_country}</p>
                    </div>
                </div>
            </div>`;
        })
        movie.innerHTML = inner;
}

//Вывести на экран ошибку
function errorViewPage(reason)
{
    movie.innerHTML = 'Упс, чт-то пошло не так!'
    console.log('error',reason)
    console.log(console.dir(reason))
}